<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_Charityname</name>
   <tag></tag>
   <elementGuidId>a8f6ab28-c8c1-489f-869c-7c50dd9f7e09</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(@class,'tile-list__tile')][1]//span[contains(text(),'Kids Helpline')][1]</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>opt_frequencyCustom</name>
   <tag></tag>
   <elementGuidId>e7a1a91a-8beb-4746-ae30-782273ed4ad2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='custom-donation row']//*[@id='frequency']//option</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='custom-donation row']//*[@id='frequency']//option</value>
   </webElementProperties>
</WebElementEntity>

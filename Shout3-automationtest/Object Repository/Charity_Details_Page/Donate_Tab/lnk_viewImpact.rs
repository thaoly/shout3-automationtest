<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lnk_viewImpact</name>
   <tag></tag>
   <elementGuidId>ab7c18e6-6c91-42ca-93da-3cab23090b79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='flex flex-wrap flex-row']/div[1]//span[contains(text(),'View impact')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='flex flex-wrap flex-row']/div[1]//span[contains(text(),'View impact')]</value>
   </webElementProperties>
</WebElementEntity>

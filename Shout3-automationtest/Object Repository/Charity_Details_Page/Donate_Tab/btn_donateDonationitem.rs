<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_donateDonationitem</name>
   <tag></tag>
   <elementGuidId>09c3a6cf-30ed-44b4-93c1-85930dd64cb7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(@class,'is-revealed')]//span[text()='Donate Now']</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>drp_frequencyCustom</name>
   <tag></tag>
   <elementGuidId>1ff6a7df-eb4c-47f8-b8cf-880cfe4405fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>.custom-donation__frequency select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='custom-donation row']//select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>.custom-donation__frequency select</value>
   </webElementProperties>
</WebElementEntity>

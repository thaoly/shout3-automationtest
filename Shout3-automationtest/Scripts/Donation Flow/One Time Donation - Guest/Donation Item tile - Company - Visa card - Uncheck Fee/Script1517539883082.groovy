import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl(findTestData('URL').getValue(1, 1))

WebUI.waitForPageLoad(GlobalVariable.G_timeOut)

WebUI.click(findTestObject('Explore_Page/GlobalSearch/icon_Search'))

WebUI.waitForElementPresent(findTestObject('Explore_Page/GlobalSearch/txt_searchbox'), GlobalVariable.G_timeOut)

WebUI.setText(findTestObject('Explore_Page/GlobalSearch/txt_searchbox'), findTestData('Donation Flow').getValue(1, 1))

WebUI.click(findTestObject('Explore_Page/GlobalSearch/btn_Search'))

txt_charityName = WebUI.getText(findTestObject('Search_Page/lbl_Charityname'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyMatch(txt_charityName, findTestData('Donation Flow').getValue(2, 1), false)

WebUI.click(findTestObject('Search_Page/lbl_Charityname'))

WebUI.waitForPageLoad(GlobalVariable.G_timeOut)

'Donate by donation item tile\r\n'
WebUI.callTestCase(findTestCase('Donation Flow/One Time Donation - Guest/Sub - Donation Flow/Sub_donationitemtile'), [('quantity') : findTestData(
            'Donation Flow').getValue(6, 1), ('frequency') : findTestData('Donation Flow').getValue(5, 1)], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForPageLoad(GlobalVariable.G_timeOut)

'Donate with donation type = Company/Organisation\r\n'
WebUI.callTestCase(findTestCase('Donation Flow/One Time Donation - Guest/Sub - Donation Flow/Sub_DonationDetails'), [('donationtype') : findTestData(
            'Donation Type').getValue(1, 2), ('firstName') : findTestData('Donation Type').getValue(2, 2), ('lastName') : findTestData(
            'Donation Type').getValue(3, 2), ('emailAddress') : findTestData('Donation Type').getValue(4, 2)], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Donation_Page/txt_companyName'), 'company')

WebUI.waitForPageLoad(GlobalVariable.G_timeOut)

'Donate via Visa card'
WebUI.callTestCase(findTestCase('Donation Flow/One Time Donation - Guest/Sub - Donation Flow/Sub_PaymentMethod'), [('creditCard') : findTestData(
            'Payment Method').getValue(2, 2), ('ccv') : findTestData('Payment Method').getValue(4, 2), ('expiryDate') : findTestData(
            'Payment Method').getValue(3, 2)], FailureHandling.STOP_ON_FAILURE)

'Donate without Fee\r\n'
WebUI.click(findTestObject('Donation_Page/cbx_fee'))

WebUI.verifyEqual(WebUI.getText(findTestObject('Donation_Page/lbl_totalAmount')), WebUI.getText(findTestObject('Donation_Page/lbl_subTotal')))

WebUI.click(findTestObject('Donation_Page/btn_donateNow'))

WebUI.waitForPageLoad(GlobalVariable.G_timeOut)

WebUI.click(findTestObject('Thank_you_Page/btn_Done'))

'Verify Transaction in SA'
WebUI.navigateToUrl(findTestData('URL').getValue(1, 3))

WebUI.waitForPageLoad(GlobalVariable.G_timeOut)

WebUI.verifyElementPresent(findTestObject('Super_Admin/Transactions_Page/tbl_transactions'), GlobalVariable.G_timeOut)

totalamount = WebUI.getText(findTestObject('Super_Admin/Transactions_Page/lbl_transactionsID'))

WebUI.verifyElementPresent(findTestObject('Super_Admin/Transactions_Page/tbl_transactions'), GlobalVariable.G_timeOut)

donor_Name = WebUI.concatenate((([findTestData('Donation Type').getValue(2, 2), ' ', findTestData('Donation Type').getValue(
                3, 2)]) as String[]), FailureHandling.STOP_ON_FAILURE)

row = CustomKeywords.'newpackage.shout.findRowContainCell'(ObjectRepository.findTestObject('Super_Admin/Transactions_Page/tbl_transactions'), 
    donor_Name, 30, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'newpackage.shout.checkTableCellValue'(ObjectRepository.findTestObject('Super_Admin/Transactions_Page/tbl_transactions'), 
    txt_charityName, row, 3, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'newpackage.shout.checkTableCellValue'(ObjectRepository.findTestObject('Super_Admin/Transactions_Page/tbl_transactions'), 
    total_Amount, row, 5, FailureHandling.STOP_ON_FAILURE)

GlobalVariable.G_TransactionID = CustomKeywords.'newpackage.shout.getTextFromTableCell'(findTestObject('Super_Admin/Transactions_Page/tbl_transactions'), 
    row, 1)

'Verify email receipt has been sent\r\n'
WebUI.callTestCase(findTestCase('Donation Flow/One Time Donation - Guest/Sub - Donation Flow/Sub_LogintoGmails'), [('email') : findTestData(
            'Donation Type').getValue(4, 2), ('password') : findTestData('Donation Flow').getValue(8, 1), ('transaction_id') : GlobalVariable.G_TransactionID
        , ('totalamount') : total_Amount], FailureHandling.STOP_ON_FAILURE)

@com.kms.katalon.core.annotation.SetUp
def Setup() {
    WebUiBuiltInKeywords.openBrowser('', FailureHandling.STOP_ON_FAILURE)

    WebUiBuiltInKeywords.maximizeWindow(FailureHandling.STOP_ON_FAILURE)
}

//@com.kms.katalon.core.annotation.TearDown
//def Teardown() {
//    WebUiBuiltInKeywords.closeBrowser(FailureHandling.STOP_ON_FAILURE)
//}import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
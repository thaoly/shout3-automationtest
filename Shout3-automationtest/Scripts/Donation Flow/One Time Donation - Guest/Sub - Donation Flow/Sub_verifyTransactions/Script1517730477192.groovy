import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.verifyElementPresent(findTestObject('Super_Admin/Transactions_Page/tbl_transactions'), GlobalVariable.G_timeOut)

donor_Name = WebUI.concatenate((([findTestData('Donation Type').getValue(2, 3),' ' ,findTestData('Donation Type').getValue(2, 3)]) as String[]), FailureHandling.STOP_ON_FAILURE)

row = CustomKeywords.'newpackage.shout.findRowContainCell'(ObjectRepository.findTestObject('Super_Admin/Transactions_Page/tbl_transactions'),
	donor_Name, 30, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'newpackage.shout.checkTableCellValue'(ObjectRepository.findTestObject('Super_Admin/Transactions_Page/tbl_transactions'),
	txt_charityName, row, 3, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'newpackage.shout.checkTableCellValue'(ObjectRepository.findTestObject('Super_Admin/Transactions_Page/tbl_transactions'),
	total_Amount, row, 5, FailureHandling.STOP_ON_FAILURE)

GlobalVariable.G_TransactionID = CustomKeywords.'newpackage.shout.getTextFromTableCell'(findTestObject('Super_Admin/Transactions_Page/tbl_transactions'),
	row, 1)

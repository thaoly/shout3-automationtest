import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUiBuiltInKeywords.navigateToUrl('mail.google.com', FailureHandling.STOP_ON_FAILURE)

WebUiBuiltInKeywords.setText(ObjectRepository.findTestObject('Gmail/txt_google_emails'), email, FailureHandling.OPTIONAL)

WebUiBuiltInKeywords.click(ObjectRepository.findTestObject('Gmail/btn_google_next'), FailureHandling.OPTIONAL)

WebUI.waitForPageLoad(GlobalVariable.G_timeOut)

WebUiBuiltInKeywords.waitForElementVisible(ObjectRepository.findTestObject('Gmail/txt_google_passwords'), 10, FailureHandling.STOP_ON_FAILURE)

WebUiBuiltInKeywords.setText(ObjectRepository.findTestObject('Gmail/txt_google_passwords'), password, FailureHandling.STOP_ON_FAILURE)

WebUiBuiltInKeywords.click(ObjectRepository.findTestObject('Gmail/btn_ggpassword_next'), FailureHandling.OPTIONAL)

WebUI.delay(GlobalVariable.G_timeOut)

WebUiBuiltInKeywords.waitForPageLoad(20, FailureHandling.STOP_ON_FAILURE)

subject = WebUI.concatenate(((['Your transaction: ', transaction_id, ' has been processed']) as String[]))

newXpath = WebUI.concatenate(((['//span//b[text()=\'', subject, '\']']) as String[]))

emailSubject = WebUI.modifyObjectProperty(findTestObject('Gmail/lbl_emailSubject'), 'xpath', 'equals', newXpath, 
    true)

WebUI.waitForElementVisible(emailSubject, GlobalVariable.G_timeOut)

WebUI.click(emailSubject)

WebUI.verifyEqual(WebUI.getText(findTestObject('Gmail /lbl_totalAmount')), totalamount)


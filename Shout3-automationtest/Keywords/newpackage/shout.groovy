package newpackage
import org.openqa.selenium.support.ui.ExpectedCondition

import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

import org.apache.commons.lang.StringUtils
import org.apache.pdfbox.*
import org.openqa.selenium.*
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.FluentWait
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.Wait
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By.ByXPath
import groovy.lang.Closure
import groovy.lang.GroovyRuntimeException
import groovy.lang.MissingPropertyException

import java.sql.Timestamp
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.HashMap
import java.util.Map
import java.util.TimeZone
import java.io.*

import com.google.common.base.Function
import com.google.common.util.concurrent.UncaughtExceptionHandlers.Exiter
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.exception.StepFailedException

import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.webui.common.EWindow
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement
import com.sun.org.omg.CORBA.ExcDescriptionSeqHelper
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

class shout {
	private static List<WebElement> findChildWebElementsOfWebElement(WebElement element, By locator) {
		List<WebElement> foundElements = null;
		try {
			foundElements = new FluentWait<WebDriver>(DriverFactory.getWebDriver()).pollingEvery(500, TimeUnit.MILLISECONDS)
					.withTimeout(RunConfiguration.getTimeOut(), TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
					.until(new Function<WebDriver, List<WebElement>>() {
						@Override
						public List<WebElement> apply(WebDriver webDriver) {
							return element.findElements(locator);
						}
					});
		} catch (TimeoutException e) {
			// timeout, do nothing
		}
		return foundElements;
	}
	
	@Keyword
	def int findRowContainCell (TestObject to, String value, int timeOut, FailureHandling flowControl){
		try{
			WebElement eTable = WebUiBuiltInKeywords.findWebElement(to, timeOut);
			List<WebElement> rows = findChildWebElementsOfWebElement (eTable, By.tagName("tr"))
			if(rows == null) return null;
			
			int size = rows.size()
			
			for(int i = 0; i < size; i++){
				WebElement row = rows.get(i)
				List<WebElement> cells = findChildWebElementsOfWebElement(row, By.xpath("./*"))
				
				if (cells == null) return null;
				int col = cells.size();
				
				for(int j = 0; j < col; j++)
				{
					String text = cells.get(j).getText();
					if (text == value) return i;
				}
			}
			
		} catch (Exception ex){
			//KeywordExceptionHandler.throwExceptionIfStepFailed(ex.getMessage(), flowControl);
		}
	}
	@Keyword
	def checkTableCellValue(TestObject tableObject, String expectedValue, int rowIndex, int columnIndex, FailureHandling FailureHandling) {
		if (expectedValue != null) {
			//Get cell value
			def actualValue = getTextFromTableCell(tableObject, rowIndex, columnIndex).toString()

			KeywordLogger.getInstance().logInfo("Actual value: '" + actualValue + "'")

			//Verify cell value
			WebUiBuiltInKeywords.verifyMatch(actualValue, expectedValue, false, FailureHandling)
		}
	}
	@Keyword
	def getTextFromTableCell(TestObject table, int rowIndex, int columnIndex) {

		//boolean isSwitchToFrame = WebUiBuiltInKeywords.switchToFrame(table)
		try {
			WebElement eTable = WebUiBuiltInKeywords.findWebElement(table,30)
			List<WebElement> rows = findChildWebElementsOfWebElement(eTable, By.tagName("tr"))
			if (rows == null || rows.size() <= rowIndex) {
				return null
			}
			WebElement row = rows.get(rowIndex)
			List<WebElement> cells = findChildWebElementsOfWebElement(row, By.xpath("./*"))
			if (cells == null || cells.size() <= columnIndex - 1) {
				return null
			}

			String text = cells.get(columnIndex - 1).getText()

			return text
		} finally {

		}
		return null
	}
}

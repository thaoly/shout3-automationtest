import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\lyhan\\AppData\\Local\\Temp\\Katalon\\Test Cases\\Donation Flow\\One Time Donation - Guest\\Sub - Donation Flow\\Sub_LogintoGmails\\20180205_174840\\execution.properties')

TestCaseMain.beforeStart()
try {
    
	    TestCaseMain.runTestCase('Test Cases/Donation Flow/One Time Donation - Guest/Sub - Donation Flow/Sub_LogintoGmails', new TestCaseBinding('Test Cases/Donation Flow/One Time Donation - Guest/Sub - Donation Flow/Sub_LogintoGmails', [:]), FailureHandling.STOP_ON_FAILURE )
    
} catch (Exception e) {
    TestCaseMain.logError(e, 'Test Cases/Donation Flow/One Time Donation - Guest/Sub - Donation Flow/Sub_LogintoGmails')
}

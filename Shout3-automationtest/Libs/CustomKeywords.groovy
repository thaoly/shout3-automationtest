
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import com.kms.katalon.core.testobject.TestObject

import java.lang.String

import com.kms.katalon.core.model.FailureHandling


def static "newpackage.shout.findRowContainCell"(
    	TestObject to	
     , 	String value	
     , 	int timeOut	
     , 	FailureHandling flowControl	) {
    (new newpackage.shout()).findRowContainCell(
        	to
         , 	value
         , 	timeOut
         , 	flowControl)
}

def static "newpackage.shout.checkTableCellValue"(
    	TestObject tableObject	
     , 	String expectedValue	
     , 	int rowIndex	
     , 	int columnIndex	
     , 	FailureHandling FailureHandling	) {
    (new newpackage.shout()).checkTableCellValue(
        	tableObject
         , 	expectedValue
         , 	rowIndex
         , 	columnIndex
         , 	FailureHandling)
}

def static "newpackage.shout.getTextFromTableCell"(
    	TestObject table	
     , 	int rowIndex	
     , 	int columnIndex	) {
    (new newpackage.shout()).getTextFromTableCell(
        	table
         , 	rowIndex
         , 	columnIndex)
}
